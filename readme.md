VDJServer-iReceptorPlus
=======================

VDJServer Repository integration with the iReceptor Plus Platform
through the Statistics and Analysis API.

##Deployments

 * Development: TBD
 * Production: TBD

The current deployment configuration has the API and database on a single node.

##Components

VDJServer-iReceptorPlus is currently composed of 2 separate components:

 * [stats-api-js-tapis](https://bitbucket.org/vdjserver/stats-api-js-tapis.git): Statistics API
 * [analysis-js-tapis](https://github.com/ireceptor-plus/analysis-js-tapis.git): Analysis API

##Configuration Procedure

There is one configuration file that needs to be set up to run the
APIs. It can be copied from its default template.

```
cp .env.defaults .env
emacs .env
```

**Configuring systemd**

You will need to set up the VDJServer-iReceptorPlus systemd service file
on your host machine in order to have the infrastructure automatically
restart when the host machine reboots. Copy the appropriate template.

```
sudo cp host/systemd/vdjserver-irplus.service /etc/systemd/system/vdjserver-irplus.service

sudo systemctl daemon-reload

sudo systemctl enable docker

sudo systemctl enable vdjserver-irplus
```

##Deployment Procedure

###SSL

VDJServer-iReceptorPlus does not handle SSL certificates directly, and
is currently configured to run HTTP. It must be deployed behind a
reverse proxy in order to allow SSL connections.

###Dockerized instances (vdj-dev, vdj-staging, production)

Dockerized instances may be started/stopped/restarted using the
supplied systemd script: host/systemd/vdjserver-irplus.service.

It is also important to note that the systemd vdjserver-irplus
command will not rebuild new container instances.

**Docker Compose Files**

There are two docker-compose files: one for general use
("docker-compose.yml"), and one that has been adjusted for use in a
production environment ("docker-compose.prod-override.yml"). These
files are meant to be overlayed and used together in a production
environment.

Example of using the production overlayed config:

```
docker-compose -f docker-compose.yml -f docker-compose.prod-override.yml build

docker-compose -f docker-compose.yml -f docker-compose.prod-override.yml up
```

##Accessing the APIs

For general users, the APIa are likely accessed with a GUI that hides
the technical details of communicating. However, it
is useful to contact manually the API using the `curl` command to
verify that the service is operational.

** Statistics API **

The top level entrypoint for the API will return a simple success status heartbeat.

```
$ curl 'https://vdj-staging.tacc.utexas.edu/irplus/v1/stats'
{"result":"success"}
```

The info entrypoint will return version and other info about the service.

```
$ curl 'https://vdj-staging.tacc.utexas.edu/irplus/v1/stats/info'
{"name":"vdjserver-ireceptor-node","description":"VDJServer API for iReceptor","version":"0.1.0"}
```

** Analysis API **

The top level entrypoint for the API will return a simple success status heartbeat.

```
$ curl 'https://vdj-staging.tacc.utexas.edu/irplus/v1/analysis'
{"result":"success"}
```

The info entrypoint will return version and other info about the service.

```
$ curl 'https://vdj-staging.tacc.utexas.edu/irplus/v1/analysis/info'
{"name":"vdjserver-ireceptor-node","description":"VDJServer API for iReceptor","version":"0.1.0"}
```

##Development Guidelines

**Code Style**

 * Code should roughly follow Google Javascript Style Guide conventions: <https://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml>.

 * Spaces are preferred over tabs, and indentation is set at 4 spaces.

 * Vimrc settings: ```set shiftwidth=4, softtabstop=4, expandtab```

**Git Structure and Versioning Process**

 * This project uses the Git Flow methodology for code management and development: <https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow>.

 * New development and features should be done on branches that are cloned from the **develop** branch, and then merged into this branch when completed. Likewise, new release candidates should be branched from **develop**, and then merged into **master** once they have been tested/verified. Once a release branch is ready for production, it should be merged into **master** and tagged appropriately. Every deployment onto production should be tagged following semantic versioning 2.0.0 standards: <http://semver.org/>.

##Development Setup

You will need to clone down the parent project and all submodules in order to set up a local instance of vdjserver.

```
- Clone project
$ git clone https://schristley@bitbucket.org/vdjserver/vdjserver-irplus.git

$ cd vdjserver-irplus

- Clone submodules (recursive as some submodules have their own submodules)
$ git submodule update --init --recursive

- Check out the appropriate branches

- Follow configuration steps listed above in the "Configuration Procedure" section of this document
```
